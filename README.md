## Adivinar un número entre 0 y 256 - Linux

Programa que averigua un número entre 0 y 256 mediante diferentes métodos de comunicación. Se han creado dos procesos separados:
```
Proceso 1: conoce el número a adivinar.
Proceso 2: intenta adivinar el número.
```

#### Setup

Entrar en los directorios y seguir los sigueintes pasos:

```
Compilar los programas con `gcc programa_X.c`.
Después, ejecutarlos con `./a.out`.
```

